from Crypto import Random
from Crypto.Cipher import AES
import base64
import hashlib
import os


class DataLayer(object):
    def __init__(self, next_layer, next_address, content):
        """:param next_layer = tpe DataLayer.
        :param next_address = tuple - ip (str), port (int).
        :param content = type string, the message to be sent."""
        self.next_layer = next_layer
        self.next_address = next_address
        self.content = content


class AESCipher(object):
    def __init__(self, key):
        """gets a key and encrypts it with sha256."""
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        """gets raw data and preforms base64 over AES encryption."""
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        """gets encrypted data of the above protocol and decrypts it."""
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s) - 1:])]


class DiffieHellmanKeyExchange:
    def __init__(self, base, prime, key_length=600):
        self.key_length = max(600, key_length)  # minimum length set to for security reasons
        self._prime = prime
        self._base = base
        self._private_key = self._generate_private_key(self.key_length)
        self.public_key = self._generate_public_key()
        self.key = None  # will be installed later.

    def _generate_private_key(self, length):
        """This function randomly generates a private key.
        NOTE: Due to the fact each private key is different (most likely) this function is NOT static."""
        _rand = 0
        _bytes = length // 8 + 8

        while _rand.bit_length() < length:
            # generating a random private key that fits minimum length.
            _rand = int(os.urandom(_bytes).encode('hex'), 16)

        return _rand

    def _generate_public_key(self):
        # generator to the power of the pre generated privet key, mod the constant prime number.
        return pow(self._base, self._private_key, self._prime)

    def generate_secret(self, public_key):
        """This function generates a joined key.
        :param public_key: type int, the received public key from the second side in the key exchange."""
        shared_secret = pow(public_key, self._private_key, self._prime)
        shared_secret_bytes = str(shared_secret).encode()
        hash_alg = hashlib.sha256()
        hash_alg.update(bytes(shared_secret_bytes))
        key = hash_alg.hexdigest()
        self.key = key
