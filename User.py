from Encapsulation_Funcstions import *
from Network_functions import *
import select
import json
import tkinter
from threading import Thread

USER_SERVER_PORT = 1054
USER_SERVER_IP = "0.0.0.0"

DIR_SERVER_PORT = 10054
DIR_SERVER_IP = "127.0.0.1"

END_USER_PORT = 666
END_USER_IP = "127.0.0.1"  # for test reasons. can't send data to LOCAL HOST address ("0.0.0.0")

BUFFER_SIZE = 4096
PATH_LENGTH = 3  # 3 or above. 3 is recommended to keep the process short.
TEXT_SIZE = 15
QUIT_CHAT = "{[quit]}"

# Key exchange values.
BASE = 2
PRIME = 2**4096 - 2**4032 - 1 + 2**64 * (2**3966 + 240904)
KEY_LENGTH = 1024

keys = []
cur_address = [USER_SERVER_IP, USER_SERVER_PORT]


def main():
    def send(event=None):
        """Handles sending of messages"""
        msg_txt = msg.get()
        if msg_txt != "":

            if msg_txt == QUIT_CHAT:
                root.quit()

            else:
                enc_msg = encapsulate_message(user_name + ": " + msg_txt, keys, path, (END_USER_IP, END_USER_PORT),
                                              PATH_LENGTH)
                data = {"type": "message", "previous_address": cur_address, "direction": 1, "data": enc_msg}
                send_data(tuple(path[0]), json.dumps(data))

                msg.set("")  # Clears input field

                msg_list.insert(tkinter.END, msg_txt + "\n")    # printing message to window

    def receive():
        """handles receiving of messages"""
        open_client_socket = []
        while True:
            rlist, wlist, xlist = select.select([server_socket] + open_client_socket, open_client_socket, [])
            for cur_sock in rlist:
                if cur_sock is server_socket:
                    (new_socket, address) = server_socket.accept()
                    open_client_socket.append(new_socket)

                else:
                    dumped_data = cur_sock.recv(BUFFER_SIZE)
                    if dumped_data:
                        data = json.loads(dumped_data)
                        if data["type"] == "message":
                            msg_list.insert(tkinter.END, data["content"] + "\n")  # printing message to window

    def on_closing(event=None):
        """This function is to be called when the GUI window is closed"""
        msg.set(QUIT_CHAT)  # quit message to server (assumed that will not be send during actual chatting).
        send()
        tkinter.Tk().quit()

    # inputs
    user_name = raw_input("User Name: ")
    # Optically, IP and PORT of end user can be entered by the user as well.
    # path_length must be 3 or above so that'll be mid nodes that have no clear information

    # running server socket
    server_socket = create_server_socket((USER_SERVER_IP, USER_SERVER_PORT))

    # Creating a socket.
    client_socket = connect_client((DIR_SERVER_IP, DIR_SERVER_PORT))
    # Getting the path from the directory server.
    path = get_path(client_socket, PATH_LENGTH)
    path = change_path(path)
    client_socket.send("{[quit]}")  # ending the connection with the server
    client_socket.close()

    # Key exchange between user and all nodes.
    key_obj = DiffieHellmanKeyExchange(BASE, PRIME, KEY_LENGTH)
    for i in range(len(path)):
        data = {"type": "key", "previous_address": cur_address, "path": path, "direction": 1,
                "data": [key_obj.public_key, BASE, PRIME]}

        # sending a key exchange request to node 0
        client_socket = connect_client(tuple(path[0]))
        client_socket.send(json.dumps(data))
        client_socket.close()

        # waiting until the last "key-less" node in the chain sends back the data through the chain in reversed order
        server_socket.listen(1)
        (node_sock, address) = server_socket.accept()
        dumped_server_data = node_sock.recv(BUFFER_SIZE)
        server_data = json.loads(dumped_server_data)
        if server_data["type"] == "key":
            public_key = server_data["data"]
            key_obj.generate_secret(public_key)
            keys.append(key_obj.key)

    # GUI

    root = tkinter.Tk()
    root.title("Encoded Chat")  # header
    messages_frame = tkinter.Frame(root)

    msg = tkinter.StringVar()  # Var for saving the msg

    # Gui message window (and chat history)
    scrollbar = tkinter.Scrollbar(messages_frame)  # initializing a Scroll Bar
    msg_list = tkinter.Listbox(messages_frame, height=15, width=50, yscrollcommand=scrollbar.set)
    scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
    msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
    msg_list.pack()
    messages_frame.pack()

    # txt field
    entry_field = tkinter.Entry(root, textvariable=msg)
    entry_field.bind("<Return>", send)
    entry_field.pack()
    # send button
    send_button = tkinter.Button(root, text="Send", command=send)
    send_button.pack()

    # opening a thread for receiving messages
    receive_thread = Thread(target=receive)
    receive_thread.setDaemon(True)  # making sure the thread will close on exit.
    receive_thread.start()

    # GUI (has to be written at the end of the main)
    root.protocol("WM_DELETE_WINDOW", on_closing)  # window close action
    tkinter.mainloop()  # Starts GUI execution

if __name__ == '__main__':
    main()
