from Classes import *
from Network_functions import *
import select
import json
import tkinter
from threading import Thread

SERVER_PORT = 666
SERVER_IP = "0.0.0.0"
BUFFER_SIZE = 1024
TEXT_SIZE = 15

QUIT_CHAT = "{[quit]}"


previous_address = None


def main():
    def send(event=None):
        """Handles sending of messages"""
        global previous_address
        msg_txt = msg.get()
        if msg_txt != "":

            if msg_txt == QUIT_CHAT:
                root.quit()

            else:
                data = {"type": "message", "direction": 0,  "content": user_name + ": " + msg_txt}
                send_data(previous_address, json.dumps(data))
                msg.set("")  # Clears input field

                msg_list.insert(tkinter.END, msg_txt + "\n")  # printing message to window

    def receive():
        global previous_address
        open_client_socket = []
        while True:
            rlist, wlist, xlist = select.select([server_socket] + open_client_socket, open_client_socket, [])
            for cur_sock in rlist:
                if cur_sock is server_socket:
                    (new_socket, address) = server_socket.accept()
                    open_client_socket.append(new_socket)

                else:
                    dumped_data = cur_sock.recv(BUFFER_SIZE)
                    if dumped_data:
                        data = json.loads(dumped_data)
                        msg_list.insert(tkinter.END, data["content"] + "\n")  # printing message to window.
                        previous_address = tuple(data["previous_address"])  # saving the sender address to return msg.

    def on_closing(event=None):
        """This function is to be called when the GUI window is closed"""
        msg.set(QUIT_CHAT)  # quit message to server (assumed that will not be send during actual chatting).
        # send(msg_count)
        tkinter.Tk().quit()  # stopping the thread.

    # inputs

    user_name = raw_input("User Name: ")  # TODO: Change to insert user name by GUI

    # running server socket
    server_socket = create_server_socket((SERVER_IP, SERVER_PORT))

    # opening a thread for receiving messages
    receive_thread = Thread(target=receive)
    receive_thread.setDaemon(True)
    receive_thread.start()

    # GUI

    root = tkinter.Tk()
    root.title("Encoded Chat")  # header
    messages_frame = tkinter.Frame(root)

    msg = tkinter.StringVar()  # Var for saving the msg.

    # Gui message window (and chat history)
    scrollbar = tkinter.Scrollbar(messages_frame)  # initializing a Scroll Bar
    msg_list = tkinter.Listbox(messages_frame, height=15, width=50, yscrollcommand=scrollbar.set)
    scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)
    msg_list.pack(side=tkinter.LEFT, fill=tkinter.BOTH)
    msg_list.pack()
    messages_frame.pack()

    # txt field
    entry_field = tkinter.Entry(root, textvariable=msg)
    entry_field.bind("<Return>", send)
    entry_field.pack()
    # send button
    send_button = tkinter.Button(root, text="Send", command=send)
    send_button.pack()

    # GUI (has to be written at the end of the main)
    root.protocol("WM_DELETE_WINDOW", on_closing)  # window close action.
    tkinter.mainloop()  # Starts GUI execution.


if __name__ == '__main__':
    main()
