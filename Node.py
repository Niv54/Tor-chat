from Encapsulation_Funcstions import *
from Network_functions import *
import select
import json


SERVER_PORT = 11116  # change between each run to create multiple different nodes.
SERVER_IP = "0.0.0.0"
CLIENT_PORT = 10054
CLIENT_IP = "127.0.0.1"

BUFFER_SIZE = 4096

KEY_LENGTH = 1024

previous_address = None
cur_address = [SERVER_IP, SERVER_PORT]


def main():
    global previous_address
    open_client_socket = []
    key = None

    # running server socket.
    server_socket = create_server_socket((SERVER_IP, SERVER_PORT))
    if server_socket:
        print "Node is running"

    # sending server information to the directory server.
    send_server_details((CLIENT_IP, CLIENT_PORT), (SERVER_IP, SERVER_PORT))

    while True:
        rlist, wlist, xlist = select.select([server_socket] + open_client_socket, open_client_socket, [])
        for cur_sock in rlist:
            if cur_sock is server_socket:
                (new_socket, address) = server_socket.accept()
                open_client_socket.append(new_socket)

            else:
                json_data = cur_sock.recv(BUFFER_SIZE)
                if json_data:
                    data = json.loads(json_data)
                    if data["type"] == "message":
                        # TODO: SEE IF NEEDED (PREV ADDRESS)
                        # checking if node has previous node address.
                        # if not previous_address:
                        #     previous_address = data["previous_address"]

                        if "direction" in data and not data["direction"]:
                            # data sent from end user.
                            send_data(previous_address, json.dumps(data))
                        else:
                            # data sent from user.
                            if key:
                                data = decapsulate(key, data["data"])
                            else:
                                print "Message sent before key exchange"

                            if "next_layer" in data:  # testing input
                                if not data["next_layer"]:
                                    # sending to end user
                                    next_address = data["next_address"]
                                    content = data["content"]
                                    data_to_send = {"type": "message", "content": content,
                                                    "previous_address": cur_address}
                                    send_data(tuple(next_address), json.dumps(data_to_send))
                                else:
                                    # sending to next node
                                    next_address = data["next_address"]
                                    next_layer = data["next_layer"]
                                    data_to_send = {"type": "message", "data": next_layer,
                                                    "previous_address": cur_address}
                                    send_data(tuple(next_address), json.dumps(data_to_send))

                    elif data["type"] == "key":
                        if not previous_address:
                            previous_address = data["previous_address"]
                        if not key:
                            # generate new key if one doesn't already exits.
                            shared_public_key, base, prime = data["data"]

                            # key exchange
                            key_obj = DiffieHellmanKeyExchange(base, prime, KEY_LENGTH)
                            data_to_send = {"type": "key", "direction": 0, "data": key_obj.public_key}
                            send_data(previous_address, json.dumps(data_to_send))

                            key_obj.generate_secret(shared_public_key)
                            key = key_obj.key
                        else:
                            # send the key details to next or previous node if key is present.
                            if not data["direction"]:

                                # sending public key to previous node.
                                send_data(tuple(previous_address), json.dumps(data))
                            else:
                                path = data["path"][1:]  # next node is first
                                data = data["data"]  # Diffie-Hellman data
                                data_to_send = {"type": "key", "previous_address": cur_address, "path": path,
                                                "direction": 1, "data": data}
                                send_data(tuple(path[0]), json.dumps(data_to_send))
                    else:
                        print "Invalid data type"
                    print "data sent"


if __name__ == '__main__':
    main()
